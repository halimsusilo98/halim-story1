from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import index,register,loginCustom,logoutCustom
from django.contrib.auth.models import User

# Create your tests here.

class TestTemplate(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_event_func(self):
        found_func = resolve('/story9/')
        self.assertEqual(found_func.func, index)

    def test_event_using_template(self):
        template = Client().get('/story9/')
        self.assertTemplateUsed(template, 'eren.html')

class TestLogIn(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/story9/login/')
        self.assertEqual(response.status_code, 200)

    def test_event_func(self):
        found_func = resolve('/story9/login/')
        self.assertEqual(found_func.func, loginCustom)

    def test_event_using_template(self):
        template = Client().get('/story9/login/')
        self.assertTemplateUsed(template, 'login.html')

    def test_signin_with_new_account(self):
        c = Client()

        user = User.objects.create(
            username='Limbad', email='mhalimss@gmail.com')
        user.set_password('Thelimbad')
        user.save()

        logged_in = c.login(username='Limbad',
                            password='Thelimbad')

        self.assertTrue(logged_in)


class TestSignUp(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/story9/register/')
        self.assertEqual(response.status_code, 200)

    def test_event_func(self):
        found_func = resolve('/story9/register/')
        self.assertEqual(found_func.func, register)

    def test_event_using_template(self):
        template = Client().get('/story9/register/')
        self.assertTemplateUsed(template, 'register.html')


class TestLogOut(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/story9/logout/')
        self.assertEqual(response.status_code, 302)

    def test_event_func(self):
        found_func = resolve('/story9/logout/')
        self.assertEqual(found_func.func, logoutCustom)

from django.shortcuts import render, redirect
from .models import Kegiatan, Peserta
from .forms import formKegiatan, formPeserta
# Create your views here.
def Akegiatan(request):
    form = formKegiatan(request.POST)
    if request.method == "POST": 
        if form.is_valid():
            form.save()
            return redirect('/story6')
    context = {
        'kegiatan':form
    }
    return render(request,'pagekegiatan.html',context)

def Apeserta(request,id_kegiatan):
    form = formPeserta(request.POST)
    if request.method == "POST": 
        if form.is_valid():
            nama = Peserta(boyKegiatan=Kegiatan.objects.get(
                id=id_kegiatan),namaPeserta = form.data['namaPeserta'])
            nama.save()
        return redirect('/story6')
    
    context = {
        'peserta':form
    }
    return render(request,'pagepeserta.html',context)

def index(request):
    kegiatan = Kegiatan.objects.all()
    peserta = Peserta.objects.all()
    context = {
        'kegiatan':kegiatan,
        'peserta':peserta
    }
    return render(request,'Story6.html',context)

def delete(request,id_kegiatan):
    Kegiatan.objects.filter(id=id_kegiatan).delete()
    return redirect('/story6')
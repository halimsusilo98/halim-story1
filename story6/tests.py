# Create your tests here.

from django.test import TestCase, Client
from django.urls import resolve
from .views import index, Akegiatan,Apeserta,delete
from .models import Kegiatan, Peserta
from .apps import Story6Config

# Create your tests here.


class TestKegiatan(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_event_index_func(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, index)

    def test_event_using_template(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'Story6.html')


class TestTambahKegiatan(TestCase):
    def test_add_event_url_is_exist(self):
        response = Client().get('/story6/create_activity/')
        self.assertEqual(response.status_code, 200)

    def test_add_event_index_func(self):
        found = resolve('/story6/create_activity/')
        self.assertEqual(found.func, Akegiatan)

    def test_add_event_using_template(self):
        response = Client().get('/story6/create_activity/')
        self.assertTemplateUsed(response, 'pagekegiatan.html')

    def test_event_model_create_new_object(self):
        kegiatan = Kegiatan(namaKegiatan="Basket")
        kegiatan.save()
        self.assertEqual(Kegiatan.objects.all().count(), 1)

    def test_event_url_post_is_exist(self):
        response = Client().post(
            '/story6/', data={'namaKegiatan': 'Programming'})
        self.assertEqual(response.status_code, 200)


class TestRegist(TestCase):
    def setUp(self):
        kegiatan = Kegiatan(namaKegiatan="Basket")
        kegiatan.save()
        self.assertEqual(Kegiatan.objects.all().count(), 1)

    def test_regist_POST(self):
        response = Client().post('/story6/create_peserta/1/',
                                 data={'namaPeserta': 'Prastawa'})
        self.assertEqual(response.status_code, 302)

    def test_regist_GET(self):
        response = self.client.get('/story6/create_peserta/1/')
        self.assertTemplateUsed(response, 'pagepeserta.html')
        self.assertEqual(response.status_code, 200)



class TestHapusKegiatan(TestCase):
    def setUp(self):
        acara = Kegiatan(namaKegiatan="Futsal")
        acara.save()
        nama = Peserta(namaPeserta="Akbar",boyKegiatan=acara)
        nama.save()

    def test_hapus_url_activity_is_exist(self):
        response = Client().post('/story6/delete/1/')
        self.assertEqual(response.status_code, 302)

class TestApp(TestCase):
    def test_app_name(self):
        self.assertEqual(Story6Config.name,'story6')
from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    namaKegiatan = models.CharField(max_length=50)
class Peserta(models.Model):
    namaPeserta = models.CharField(max_length=50)
    boyKegiatan = models.ForeignKey(Kegiatan,on_delete=models.CASCADE)

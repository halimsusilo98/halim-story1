from django.urls import path
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('create_activity/', views.Akegiatan, name='Akegiatan'),
    path('create_peserta/<int:id_kegiatan>/', views.Apeserta, name='Apeserta'),
    path('delete/<int:id_kegiatan>/', views.delete, name='delete'),
]
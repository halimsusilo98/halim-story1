from django import forms
from .models import Kegiatan,Peserta
from django.forms import ModelForm

class formKegiatan(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = [
            'namaKegiatan'
        ]
        widgets = {
            'namaKegiatan'  :   forms.TextInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder' : 'Contoh: Basket',
                    'required' : True,
                }
            )
        }

class formPeserta(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = [
            'namaPeserta',
        ]
        widgets = {
            'namaPeserta'  :   forms.TextInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder' : 'Masukkan Nama Anda',
                    'required' : True,
                }
            )
        }
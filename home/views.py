from django.shortcuts import render, redirect
from .models import inputan as tabel
from .forms import inputanForm

# Create your views here.
def index(request):
    return render(request, 'home/index.html')
def quote(request):
    return render(request, 'home/page2.html')
def Join(request):
    # form = inputanForm(request.POST)
    if request.method == "POST": 
        form = inputanForm(request.POST)
        if form.is_valid():
            inp = tabel()
            inp.Mata_Kuliah = form.cleaned_data['Mata_Kuliah']
            inp.Dosen = form.cleaned_data['Dosen']
            inp.SKS = form.cleaned_data['SKS']
            inp.Semester = form.cleaned_data['Semester']
            inp.Kelas = form.cleaned_data['Kelas']
            inp.Deskripsi = form.cleaned_data['Deskripsi']
            inp.save()
            return redirect('/inputan')

    inp = tabel.objects.all()
    form = inputanForm()
    response = {"inp":inp, 'form' : form}
    return render(request,'home/inputan.html',response)

def inp_delete(request,pk):
    # form = inputanForm(request.POST)
    if request.method == "POST":
        form = inputanForm(request.POST)
        if form.is_valid():
            inp = tabel()
            inp.Day = form.cleaned_data['Mata_Kuliah']
            inp.Date = form.cleaned_data['Dosen']
            inp.Time = form.cleaned_data['SKS']
            inp.Name = form.cleaned_data['Semester']
            inp.Location = form.cleaned_data['Kelas']
            inp.Unknown = form.cleaned_data['Deskripsi']
            inp.save()
            return redirect('/inputan')

    tabel.objects.filter(pk=pk).delete()
    data = tabel.objects.all()
    form = inputanForm()
    response = {"inp":data,'form':form}
    return redirect('/inputan')

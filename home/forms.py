from django import forms
from .models import inputan
from django.forms import ModelForm

class inputanForm(forms.ModelForm):
    class Meta:
        model = inputan
        fields=[
            'Mata_Kuliah',
            'Dosen',
            'SKS',
            'Semester',
            'Kelas',
            'Deskripsi'
        ]
        widgets = {
            'Mata_Kuliah'  :   forms.TextInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder' : 'Mata Kuliah',
                    'type' : 'text',
                    'required' : True,
                }
            ),
            'Dosen'  :   forms.TextInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder' : 'Dosen',
                    'type' : 'text',
                    'required' : True,
                }
            ),
            'SKS'  :   forms.TextInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder' : 'SKS',
                    'type' : 'text',
                    'required' : True,
                }
            ),
            'Semester'  :   forms.TextInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder' : 'Semester',
                    'type' : 'text',
                    'required' : True,
                }
            ),
            'Kelas'  :   forms.TextInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder' : 'Kelas',
                    'type' : 'text',
                    'required' : True,
                }
            ),
            'Deskripsi'  :   forms.TextInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder' : 'Deskripsi',
                    'type' : 'text',
                    'required' : True,
                }
            )

        }
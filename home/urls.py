from django.urls import path
from .views import Join, inp_delete
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('quote', views.quote, name='quote'),
    path('inputan', views.Join, name='inputan'),
    path('<int:pk>', inp_delete, name='Delete'),
]
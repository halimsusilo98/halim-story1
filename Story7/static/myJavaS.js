$(".reorder-up").click(function () {
    var $current = $(this).closest('.card')
    var $previous = $current.prev('.card');
    if ($previous.length !== 0) {
      $current.delay(100000).insertBefore($previous);
    }
    return false;
  });

  $(".reorder-down").click(function () {
    var $current = $(this).closest('.card')
    var $next = $current.next('.card');
    if ($next.length !== 0) {
        $current.insertAfter($next);
    }
    return false;
  });
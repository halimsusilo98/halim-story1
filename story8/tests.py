from django.test import TestCase,Client
from django.urls import resolve
from .views import index,tampil
# Create your tests here.

class Testing(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_event_index_func(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, index)

    def test_event_using_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'mine.html')

class test_lib(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/story8/res/?q=books')
        self.assertEqual(response.status_code, 200)

